using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore.Text;
using UnityEngine.XR;

public class CaracterDroneSwitch : MonoBehaviour
{
    public GameObject character;
    public GameObject drone;
   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (character.activeSelf)
            {
                character.SetActive(false);
                drone.SetActive(true);
            }
            else
            {
                character.SetActive(true);
                drone.SetActive(false);
            }
        }
    }
}



